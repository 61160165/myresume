import 'package:flutter/material.dart';

void main() {
  runApp(myResume());
}

Column _iconImages(String image) {
  return Column(
    children: [
      Image.asset(
        image,
        width: 50,
        height: 50,
      ),
    ],
  );
}

class myResume extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget infoSection = Container(
      padding: EdgeInsets.only(top: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Information ',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          Text(
            'Name: Wirakorn Yeukyen.',
            style: TextStyle(fontSize: 18),
          ),
          Text(
            'Age: 12.   Gender: Male.',
            style: TextStyle(fontSize: 18),
          ),
          Text(
            'Email: 61160165@go.buu.ac.th',
            style: TextStyle(fontSize: 18),
          ),
          Text(
            'Hobby: play game, read book',
            style: TextStyle(fontSize: 18),
          ),
        ],
      ),
    );
    Widget skillSection = Container(
      padding: EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            'Skill:  ',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          _iconImages('images/html_Icon.png'),
          _iconImages('images/css_Icon.png'),
          _iconImages('images/js_Icon.png'),
          _iconImages('images/java_Icon.png')
        ],
      ),
    );
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Resume'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/profile_blackglasses.png',
              width: 75,
              height: 120,
            ),
            infoSection,
            skillSection
          ],
        ),
      ),
    );
  }
}
